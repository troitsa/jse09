package ru.vlasova.iteco.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.IProjectRepository;
import ru.vlasova.iteco.taskmanager.entity.Project;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    @Nullable
    public String getIdByIndex(@NotNull String userId, int projectIndex) {
        @Nullable final List<Project> projectList = findAll(userId);
        @Nullable final Project project = projectList.get(projectIndex);
        if (project == null) return null;
        @NotNull final String projectId = project.getId();
        return projectId;
    }

    @Override
    @NotNull
    public List<Project> search(@NotNull String userId, @NotNull String searchString) {
        @Nullable final List<Project> projectList = findAll(userId);
        @NotNull final List<Project> projectSearch = new ArrayList<>();
        for(@NotNull Project project : projectList) {
            if(project.getName().contains(searchString) || project.getDescription().contains(searchString)) {
                projectSearch.add(project);
            }
        }
        return projectSearch;
    }

}
