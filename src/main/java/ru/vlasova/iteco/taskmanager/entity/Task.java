package ru.vlasova.iteco.taskmanager.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.enumeration.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractEntity  {

    @Nullable
    private String name = "";

    @Nullable
    private String description = "";

    @NotNull
    private Date dateCreate = new Date(System.currentTimeMillis());

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @Nullable
    private String projectId = "";

    @NotNull
    private Status status = Status.PLANNED;

    public Task(@NotNull final String userId) {
        this.userId = userId;
    }

    @Override
    @NotNull
    public String toString() {
        return "    Task " + name;
    }



}
