package ru.vlasova.iteco.taskmanager.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.enumeration.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractEntity {

    @Nullable
    private String name;

    @Nullable
    private String description;

    @NotNull
    private Date dateCreate = new Date(System.currentTimeMillis());

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @NotNull
    private Status status = Status.PLANNED;

    public Project(@NotNull final String userId) {
        this.userId = userId;
    }

    @Override
    @NotNull
    public String toString() {
        return "Project " + name + ". Start: " + dateStart + ". Finish: " + dateFinish;
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        @Nullable final Project project = (Project) o;
        if(name == null) return false;
        return name.equals(project.getName());
    }

}