package ru.vlasova.iteco.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

public interface IUserService extends IService<User> {

    @Nullable
    User insert(@Nullable final String login, @Nullable final String password);

    @Nullable
    User login(@Nullable final String login, @Nullable final String password);

    @Nullable
    String checkUser(@Nullable final String login);

    void edit(@Nullable final User user, @Nullable final String login, @Nullable final String password);

    boolean checkRole(@Nullable final String userId, @Nullable final Role[] roles);

}
