package ru.vlasova.iteco.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.TerminalService;


public final class UserEditCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    @NotNull
    public Role[] getRole() {
        return new Role[] {Role.USER, Role.ADMIN};
    }

    @Override
    @NotNull
    public String getName() {
        return "user_edit";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Edit user";
    }

    @Override
    public void execute() {
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final User user = serviceLocator.getCurrentUser();
        if (user == null) {
            terminalService.print("You are not authorized");
            return;
        }
        terminalService.print("Edit user. Set new login: ");
        @Nullable final String login = terminalService.readString();
        terminalService.print("Set new password: ");
        @Nullable final String password = terminalService.readString();
        userService.edit(user, login, password);
    }

}