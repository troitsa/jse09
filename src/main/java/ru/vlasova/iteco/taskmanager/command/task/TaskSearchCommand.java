package ru.vlasova.iteco.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.util.List;
import java.util.stream.Stream;

public class TaskSearchCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    @NotNull
    public Role[] getRole() {
        return new Role[]{Role.USER};
    }

    @Override
    public @NotNull String getName() {
        return "task_search";
    }

    @Override
    public @NotNull String getDescription() {
        return "Search task by name or description";
    }

    @Override
    public void execute() {
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final String userId = serviceLocator.getCurrentUserId();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        terminalService.print("Search task: ");
        @Nullable final String search = terminalService.readString();
        @Nullable final List<Task> taskList = taskService.search(userId, search);
        if(taskList.isEmpty()) {
            terminalService.print("No matches");
            return;
        }
        Stream stream = taskList.stream();
        stream.forEach(e -> terminalService.print(e.toString()));
    }

}
