package ru.vlasova.iteco.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.IProjectService;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.util.List;
import java.util.stream.Stream;

public class ProjectSearchCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    @NotNull
    public Role[] getRole() {
        return new Role[] {Role.USER};
    }

    @Override
    public @NotNull String getName() {
        return "project_search";
    }

    @Override
    public @NotNull String getDescription() {
        return "Search project by name or description";
    }

    @Override
    public void execute() {
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final String userId = serviceLocator.getCurrentUserId();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        terminalService.print("Search projects: ");
        @Nullable final String search = terminalService.readString();
        @Nullable final List<Project> projectList = projectService.search(userId, search);
        if(projectList.isEmpty()) {
            terminalService.print("No matches");
            return;
        }
        Stream stream = projectList.stream();
        stream.forEach(e -> terminalService.print(e.toString()));
    }

}
