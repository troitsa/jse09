package ru.vlasova.iteco.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.IProjectService;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;
import ru.vlasova.iteco.taskmanager.service.TerminalService;


public final class ProjectCreateCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    @NotNull
    public Role[] getRole() {
        return new Role[] {Role.USER};
    }

    @Override
    @NotNull
    public String getName() {
        return "project_create";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Create new project";
    }

    @Override
    public void execute() throws DuplicateException {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @Nullable final String userId = serviceLocator.getCurrentUserId();
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        if (userId == null) {
            terminalService.print("You are not authorized");
            return;
        }
        terminalService.print("Creating project. Set name: ");
        @Nullable final String name = terminalService.readString();
        terminalService.print("Input description: ");
        @Nullable final String description = terminalService.readString();
        terminalService.print("Set start date: ");
        @Nullable final String dateStart = terminalService.readString();
        terminalService.print("Set end date: ");
        @Nullable final String dateFinish = terminalService.readString();
        @Nullable final Project project = projectService.insert(userId, name, description, dateStart, dateFinish);
        if(project == null) return;
        projectService.persist(project);
        terminalService.print("Project created.");
    }

}