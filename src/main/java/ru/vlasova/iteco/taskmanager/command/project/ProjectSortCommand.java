package ru.vlasova.iteco.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.IProjectService;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public final class ProjectSortCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    @NotNull
    public Role[] getRole() {
        return new Role[] {Role.USER};
    }

    @Override
    @NotNull
    public String getName() {
        return "project_sort";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Sorting projects";
    }

    @Override
    public void execute() {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final String userId = serviceLocator.getCurrentUserId();
        if(userId == null) return;
        @Nullable final List<Project> projectList = projectService.findAll(userId);
        if(projectList == null || projectList.isEmpty()) {
            terminalService.print("There are no projects. To create: project_create");
            return;
        }
        terminalService.print("Choose sorting option:\n" +
                "1 sorting by creating date\n"+
                "2 sorting by starting date\n"+
                "3 sorting by finish date\n"+
                "4 sorting by ready condition\n"+
                "or leave the field empty");
        @Nullable final String sortMode = terminalService.readString();
        @Nullable Comparator<Project> comparator = (o1, o2) -> o1.getName().compareTo(o2.getName());

        switch (sortMode) {
            case ("1"):
                comparator = (o1, o2) -> o1.getDateCreate().compareTo(o2.getDateCreate());
                break;
            case ("2"):
                comparator = (o1, o2) -> o1.getDateStart().compareTo(o2.getDateStart());
                break;
            case ("3"):
                comparator = (o1, o2) -> o1.getDateFinish().compareTo(o2.getDateFinish());
                break;
            case ("4"):
                comparator = (o1, o2) -> o1.getStatus().compareTo(o2.getStatus());
                break;
        }
        Stream stream = projectList.stream();
        stream.sorted(comparator).forEach(e -> terminalService.print(e.toString()));
    }

}
