package ru.vlasova.iteco.taskmanager.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.IProjectService;
import ru.vlasova.iteco.taskmanager.api.service.ServiceLocator;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.io.IOException;
import java.util.List;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public void setServiceLocator(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract boolean secure();

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws IOException, DuplicateException;

    @Nullable
    public Role[] getRole() {
        return null;
    }

    public void printProjectList(@Nullable final String userId) {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final List<Project> projectList = projectService.findAll(userId);
        if (projectList == null || projectList.size() == 0) {
            terminalService.print("There are no projects.");
            return;
        }
        int i = 1;
        for (@NotNull Project project : projectList) {
            terminalService.print(i++ + ": " + project);
        }
    }

    public void printTaskList(@Nullable final List<Task> taskList) {
        int i = 0;
        if (taskList == null) return;
        for (@Nullable Task task : taskList) {
            serviceLocator.getTerminalService().print(i++ + ": " + task);
        }
    }

}